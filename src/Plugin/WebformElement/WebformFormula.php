<?php

namespace Drupal\webform_calculator\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\NumericBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use \Webit\Util\EvalMath\EvalMath;

/**
 * Provides a 'webform_formula' element.
 *
 * @WebformElement(
 *   id = "webform_formula",
 *   label = @Translation("Formula"),
 *   description = @Translation("Computes values of other fields."),
 *   category = @Translation("Advanced elements"),
 * )
 */
class WebformFormula extends NumericBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return parent::getDefaultProperties() + [
      'formula' => '',
      'error_message' => '',
      'precision' => 2,
      'separator' => '',
      'point' => ',',
      'hidden' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['webform_formula'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Formula settings'),
    ];

    // @todo.
    // $allowed_element_types = \Drupal::config('webform_calculator.settings')->get('settings.allowed_element_types');
    // Create description for formula value.
    $parser = new EvalMath();
    $example_tokens = [
      '[webform_submission:values:some_select:key]',
      '[webform_submission:values:some_numericfield',
      '[webform_submission:values:some_radios:key]',
    ];
    $description[] = '<em>' . $this->t('Enter the calculation formula using submission tokens.') . '</em>';
    $allowed_operators = ['+', '-', '*', '/', '^', '_'];
    $description[] = '<strong>' . $this->t('Operators:') . '</strong> ' . implode(', ', $allowed_operators);
    $description[] = '<strong>' . $this->t('Constants:') . '</strong> ' . implode(', ', $parser->vb);
    $description[] = '<strong>' . $this->t('Functions:') . '</strong> ' . implode(', ', $parser->fb);
    $description[] = '<strong>' . $this->t('Example:') . "</strong> ({$example_tokens[0]} + {$example_tokens[1]}) ^ {$example_tokens[2]}";

    // $form_state->get('form_element_you_want')
    $form['webform_formula']['formula'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Formula value'),
      '#description' => implode('<br />', $description),
      '#required' => TRUE,
      '#rows' => 3,
      '#placeholder' => ['placeholder' => "{$example_tokens[0]} + {$example_tokens[1]}"],
      '#parents' => ['webform_formula', 'formula'],
    ];

    $form['webform_formula']['error_message'] = [
      '#type' => 'textfield',
      '#title' => t('Error message'),
      '#description' => $this->t('This message will be displayed when values of components inside formula will be empty or incorrect. By default user will see <strong>Enter correct value for %fields to see result</strong> message. Leave this field empty to use default message.'),
      '#parents' => ['webform_formula', 'error_message'],
    ];

    $form['webform_formula']['precision'] = [
      '#type' => 'select',
      '#title' => t('Precision'),
      '#description' => t('Number of significant digits (after the decimal point).'),
      '#options' => range(0, 10),
      '#parents' => ['webform_formula', 'precision'],
    ];

    $form['webform_formula']['separator'] = [
      '#type' => 'select',
      '#title' => t('Thousands separator'),
      '#options' => [
        ',' => t('Comma (,)'),
        '.' => t('Period (.)'),
        ' ' => t('Space ( )'),
        '' => t('None'),
      ],
      '#parents' => ['webform_formula', 'separator'],
    ];

    $form['webform_formula']['point'] = [
      '#type' => 'select',
      '#title' => t('Decimal point'),
      '#options' => [',' => t('Comma (,)'), '.' => t('Period (.)')],
      '#parents' => ['webform_formula', 'point'],
    ];

    // @todo.
    // $form['webform_formula']['hidden'] = [
    // '#type' => 'checkbox',
    // '#title' => t('Hidden'),
    // '#description' => t('Hide result of this formula. This will hide component on form but will save result to database.'),
    // ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission) {
    parent::prepare($element, $webform_submission);
    $element['#element_validate'][] = [get_class($this), 'validateFormula'];
  }

  /**
   * Form API callback. Removes ignored element for $form_state values.
   */
  public static function validateFormula(array &$element, FormStateInterface $form_state, array &$completed_form) {
    // @todo.
    // Perhaps it would make sense to perform some sort of validation here,
    // allthough that's not so easy as we cannot apply the tokens yet, there
    // must be a way to check at least if the used values are in form state
    // and are valid.
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(array &$element, WebformSubmissionInterface $webform_submission) {
    // Apply tokens.
    $formula = $this->tokenManager->replace($element['#formula'], $webform_submission);

    // Evaluate formula and fill value.
    $data = $webform_submission->getData();

    try {
      $data[$element['#webform_key']] = $this->evaluate($formula, $element['#precision'], $element['#separator'], $element['#point']);
      $webform_submission->setData($data);
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('Error occured: @error', array('@error' => $e->getMessage())), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $properties = $this->getConfigurationFormProperties($form, $form_state);

    $original_formula = $properties['#formula'];
    $formula = $properties['#formula'];

    // Verify formula syntax.
    // Replace elements in [].
    $formula = preg_replace(WEBFORM_CALCULATOR_REGEX, '(1)', $formula);

    try {
      $this->evaluate($formula);
    }
    catch (\Exception $e) {
      // Div by zero isn't a syntax error, might have just been caused by our
      // arbitrary variable substitution.
      if ($e->getMessage() != 'division by zero') {
        set_error_handler('_webform_entity_element_validate_rendering_error_handler');
        $form_state->setErrorByName('formula', t('The formula syntax @formula is invalid.<br />Evaluation returned with error: @error.', [
          '@formula' => $original_formula,
          '@error' => $e->getMessage(),
        ]));
        set_error_handler('_drupal_error_handler');
      }
    }
  }

  /**
   * Evaluate math formula and format result.
   *
   * @param string $formula
   *   Mathematical formula (without tokens).
   * @param int $precision
   *   Round precision.
   * @param string $separator
   *   Decimal separator.
   * @param string $point
   *   Grouping separator.
   *
   * @return float
   *   Calculated value for given formula.
   */
  private function evaluate($formula, $precision = 1, $separator = '', $point = '.') {
    $parser = new EvalMath();

    // The EvalMath php class can have issues with UTF-8 encoding and nbsp.
    $formula = str_replace("\xA0", ' ', iconv("UTF-8", "ISO-8859-1//TRANSLIT", $formula));

    // Handle errors externally.
    $parser->suppress_errors = TRUE;
    $result = $parser->evaluate($formula);

    if (isset($parser->last_error)) {
      throw new \Exception($parser->last_error);
    }

    $result = (string) round($result, $precision);
    if ($separator || $point != '.') {
      $pieces = explode($point, $result);
      $pieces[0] = number_format($pieces[0], 0, '.', $separator);
      $result = implode($point, $pieces);
    }
    return $result;
  }

}
