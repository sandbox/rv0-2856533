<?php

namespace Drupal\webform_calculator\Element;

use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a webform element for formula calculation.
 *
 * @code
 * $form['webform_formula'] = array(
 *   '#type' => 'webform_formula',
 *   '#title' => $this->t('Formula'),
 *   '#default_value' => ''
 * );
 * @endcode
 *
 * @FormElement("webform_formula")
 */
class WebformFormula extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
    ];
  }

}
