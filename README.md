Installation and configuration
------------------------------
1. Install Webform Calculator (webform_calculator) module and all required
   modules as usual (https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-8).
   Run composer update.
2. Create a webform.
3. Create a few webform fields that contains numeric data.
4. Create a webform formula field. Use tokens to access other form elements.
5. View this webform and fill newly added fields.


Usage
-----
Available operators:
+, -, *, /, (, ), %